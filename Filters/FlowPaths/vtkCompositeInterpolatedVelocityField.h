/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkCompositeInterpolatedVelocityField.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkCompositeInterpolatedVelocityField
 * @brief   An abstract class for
 *  obtaining the interpolated velocity values at a point
 *
 *  vtkCompositeInterpolatedVelocityField acts as a continuous velocity field
 *  by performing cell interpolation on one or more underlying vtkDataSets. That is,
 *  composite datasets are combined to create a continuous velocity field.
 *
 * @warning
 *  vtkCompositeInterpolatedVelocityField is not thread safe. A new instance
 *  should be created by each thread.
 *
 * @sa
 *  vtkInterpolatedVelocityField vtkCellLocatorInterpolatedVelocityField
 *  vtkGenericInterpolatedVelocityField vtkCachingInterpolatedVelocityField
 *  vtkTemporalInterpolatedVelocityField vtkFunctionSet vtkStreamTracer
 */

#ifndef vtkCompositeInterpolatedVelocityField_h
#define vtkCompositeInterpolatedVelocityField_h

#include "vtkAbstractInterpolatedVelocityField.h"
#include "vtkFiltersFlowPathsModule.h" // For export macro

#include <vector> // STL Header; Required for vector

class vtkDataSet;
class vtkDataArray;
class vtkPointData;
class vtkGenericCell;
class vtkCompositeInterpolatedVelocityFieldDataSetsType;

class VTKFILTERSFLOWPATHS_EXPORT vtkCompositeInterpolatedVelocityField
  : public vtkAbstractInterpolatedVelocityField
{
public:
  vtkTypeMacro(vtkCompositeInterpolatedVelocityField, vtkAbstractInterpolatedVelocityField);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Add a dataset for implicit velocity function evaluation. If more than
   * one dataset is added, the evaluation point is searched in all until a
   * match is found. THIS FUNCTION DOES NOT CHANGE THE REFERENCE COUNT OF
   * dataset FOR THREAD SAFETY REASONS.
   */
  virtual void AddDataSet(vtkDataSet* dataset);

  using Superclass::FunctionValues;
  /**
   * Evaluate the velocity field f at point (x, y, z).
   */
  int FunctionValues(double* x, double* f) override;

  /**
   * Project the provided point on current cell, current dataset.
   */
  virtual int SnapPointOnCell(double* pOrigin, double* pProj);

  /**
   * Set the cell id cached by the last evaluation within a specified dataset.
   */
  void SetLastCellId(vtkIdType c, int dataindex) override;

  /**
   * Set the cell id cached by the last evaluation.
   */
  void SetLastCellId(vtkIdType c) override { this->Superclass::SetLastCellId(c); }

  ///@{
  /**
   * Get the most recently visited dataset and its id. The dataset is used
   * for a guess regarding where the next point will be, without searching
   * through all datasets. When setting the last dataset, care is needed as
   * no reference counting or checks are performed. This feature is intended
   * for custom interpolators only that cache datasets independently.
   */
  vtkGetMacro(LastDataSetIndex, int);
  ///@}

  /**
   * Copy essential parameters between instances of this class. See
   * vtkAbstractInterpolatedVelocityField for more information.
   */
  void CopyParameters(vtkAbstractInterpolatedVelocityField* from) override;

protected:
  vtkCompositeInterpolatedVelocityField();
  ~vtkCompositeInterpolatedVelocityField() override;

  /**
   * Evaluate the velocity field f at point (x, y, z) in a specified dataset
   * by either involving vtkPointLocator, via vtkPointSet::FindCell(), in
   * locating the next cell (for datasets of type vtkPointSet) or simply
   * invoking vtkImageData::FindCell() or vtkRectilinearGrid::FindCell() to
   * fulfill the same task if the point is outside the current cell.
   */
  int FunctionValues(vtkDataSet* ds, double* x, double* f) override
  {
    return this->Superclass::FunctionValues(ds, x, f);
  }

  int LastDataSetIndex;
  std::vector<vtkDataSet*> DataSets;

private:
  vtkCompositeInterpolatedVelocityField(const vtkCompositeInterpolatedVelocityField&) = delete;
  void operator=(const vtkCompositeInterpolatedVelocityField&) = delete;
};

#endif
